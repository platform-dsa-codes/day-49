import java.util.Stack;

class Solution {
    public int largestRectangleArea(int[] heights) {
        Stack<Integer> stack = new Stack<>();
        int maxArea = 0;
        int i = 0;
        while (i < heights.length) {
            if (stack.isEmpty() || heights[i] >= heights[stack.peek()]) {
                stack.push(i);
                i++;
            } else {
                int top = stack.pop();
                int width = stack.isEmpty() ? i : i - stack.peek() - 1;
                maxArea = Math.max(maxArea, heights[top] * width);
            }
        }
        while (!stack.isEmpty()) {
            int top = stack.pop();
            int width = stack.isEmpty() ? i : i - stack.peek() - 1;
            maxArea = Math.max(maxArea, heights[top] * width);
        }
        return maxArea;
    }
}

public class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();
        
        // Example 1
        int[] heights1 = {2, 1, 5, 6, 2, 3};
        System.out.println("Example 1 Output: " + solution.largestRectangleArea(heights1)); // Output: 10
        
        // Example 2
        int[] heights2 = {2, 4};
        System.out.println("Example 2 Output: " + solution.largestRectangleArea(heights2)); // Output: 4
    }
}
